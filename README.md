Anony-forum!
============

### Tópicos ativos ###
Nosso fórum é totalmente volátil. A versão atual está consolidada [**aqui**](https://bitbucket.org/felipegte/anony-forum/src/1a72e39037fbe7841094eeadbc61ebc2bb132770/Forum/?at=master).

### Contribuição ###
Para contribuir, siga as simples instruções abaixo:

 - Criar fóruns novos
         - Crie uma nova branch do Repo
	 - Faça um clone de seu Repo e adicione um novo diretório na raiz do projeto com o nome do Fórum
	 - **Nos envie um PullRequest**
	 - Profit!
 - Criar tópicos novos
	 - Crie uma nova branch do Repo
	 - Faça um clone de seu Repo e escolha um dos Fóruns (aka: diretórios) existentes cujo tópico esteja relacionado ao assunto que deseja tratar
	 - Crie um novo arquivo **.md** neste Fórum com o texto que dará início ao debate
	 - **Nos envie um PullRequest**
	 - Profit!
 - Responder a um tópico
	 - Apenas comente livremente no arquivo/tópico de interesse.
		 - *Protip*: Comente livremente nas linhas de interesse.

### Outros links de interesse ###
1. [NWay Projects - github](https://github.com/nway-projects)
2. NWay Hangouts
3. NWay Facebook