Anony-forum!
============


### Contribuição ###
Para contribuir, siga as simples instruções abaixo:

 - Criar fóruns novos
         - Crie uma nova branch do Repo
	 - Faça um clone de seu Repo e adicione um novo diretório na raiz do projeto com o nome do Fórum
	 - **Nos envie um PullRequest**
	 - Profit!
 - Criar tópicos novos
	 - Crie uma nova branch do Repo
	 - Faça um clone de seu Repo e escolha um dos Fóruns (aka: diretórios) existentes cujo tópico esteja relacionado ao assunto que deseja tratar
	 - Crie um novo arquivo **.md** neste Fórum com o texto que dará início ao debate
	 - **Nos envie um PullRequest**
	 - Profit!
 - Responder a um tópico
	 - Apenas comente livremente no arquivo/tópico de interesse.
		 - *Protip*: Comente livremente nas linhas de interesse.
